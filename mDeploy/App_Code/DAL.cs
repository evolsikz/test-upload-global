﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

using mDeploy.App_Code;

namespace mDeploy.App_Code
{
    public class DAL : IDisposable
    {
        private SqlConnection c_connection;
        private string c_connection_string;
        private string c_connection_ip;
        private Utill c_util;

        public DAL()
        {
            this.c_connection = new SqlConnection();
            this.c_connection_string = "Data Source={0}; User Id=sa; Password=pTT!CT01; MultipleActiveResultSets=True;";

            this.c_util = new Utill();
        }

        public string Connection_IP
        {
            get { 
                return this.c_connection_ip; 
            }
            set { 
                this.c_connection_ip = value;
            }
        }

        public void Connection_Open()
        {
            try
            {
                string l_conn_string = string.Format(this.c_connection_string, this.c_connection_ip);
                this.c_connection.ConnectionString = l_conn_string;
                if (this.c_connection.State == System.Data.ConnectionState.Open)
                {
                    this.c_connection.Close();
                }
                this.c_connection.Open();

                l_conn_string = default;
            }
            catch
            { 
                
            }
        }

        public bool Connection_Open_Result()
        {
            bool l_result = false;

            try
            {

                string l_conn_string = string.Format(this.c_connection_string, this.c_connection_ip);
                this.c_connection.ConnectionString = l_conn_string;
                if (this.c_connection.State == System.Data.ConnectionState.Open)
                {
                    this.c_connection.Close();
                }
                this.c_connection.Open();

                l_result = true;
                l_conn_string = default;

                return l_result;
            }
            catch
            {
                return false;
            }
            finally
            {
                l_result = default;
            }
        }

        public void Connection_Close()
        {
            try
            {
                if (this.c_connection.State != System.Data.ConnectionState.Closed)
                {
                    this.c_connection.Close();
                }
            }
            catch
            {

            }
        }

        private DataTable LoadTable(string p_query, SqlConnection p_conn = null, SqlTransaction p_tran = null, int p_time_out = 120)
        {
            DataTable l_dt_result;
            SqlDataAdapter l_da;

            try
            {
                l_dt_result = new DataTable();

                if (p_conn == null)
                {
                    p_conn = this.c_connection;
                }

                if (p_conn.State != System.Data.ConnectionState.Open)
                {
                    p_conn.Open();
                }

                l_da = new SqlDataAdapter(p_query, p_conn);

                if (p_tran != null)
                {
                    l_da.SelectCommand.Transaction = p_tran;
                }

                l_da.Fill(l_dt_result);

                return l_dt_result;
            }
            catch
            {
                return null;
            }
            finally
            {
                l_dt_result = default;
                l_da = default;
            }
        }

        private string ExecSQL(string p_query, SqlConnection p_conn = null, SqlTransaction p_tran = null)
        {
            SqlCommand l_comm;

            try
            {
                if (p_conn == null)
                {
                    p_conn = this.c_connection;
                }

                if (p_conn.State != System.Data.ConnectionState.Open)
                {
                    p_conn.Open();
                }

                l_comm = new SqlCommand(p_query, p_conn);
                l_comm.CommandTimeout = 120;

                if (p_tran != null)
                {
                    l_comm.Transaction = p_tran;
                }

                l_comm.ExecuteNonQuery();
                return "OK";
            }
            catch(Exception ex)
            {
                return "Error! ExecSQL : " + p_query + " Message : " + ex.Message;
            }
            finally
            {
                l_comm = default;
            }
        }

        public DataTable GetStation()
        {
            DataTable l_dt_result = new DataTable();

            try
            {
                string l_query = "";
                l_query += "SELECT * FROM mDeployDB.dbo.TBStation ORDER BY Station_ID ASC";

                l_dt_result = LoadTable(l_query);

                l_query = default;

                return l_dt_result;
            }
            catch
            {
                return null;
            }
            finally
            {
                l_dt_result = default;
            }
        }

        public DataTable GetApp_Data()
        {
            DataTable l_dt_result = new DataTable();

            try
            {
                string l_query = "";
                l_query += "SELECT * FROM POSDB.dbo.APP_DATA";

                l_dt_result = LoadTable(l_query);

                l_query = default;

                return l_dt_result;
            }
            catch
            {
                return null;
            }
            finally
            {
                l_dt_result = default;
            }
        }

        public DataTable GetIsCoco()
        {
            DataTable l_dt_result = new DataTable();

            try
            {
                string l_query = "";
                l_query += "SELECT CONFIG_VALUE FROM POSDB.dbo.APP_CONFIG WHERE CONFIG_KEY = 'HQ_SERVICE_EXPORT'";

                l_dt_result = LoadTable(l_query);

                l_query = default;

                return l_dt_result;
            }
            catch
            {
                return null;
            }
            finally
            {
                l_dt_result = default;
            }
        }

        public DataTable GetVersion(string p_bussiness_type)
        {
            DataTable l_dt_result = new DataTable();

            try
            {
                string l_query = "";

                if (p_bussiness_type == "OIL")
                {
                    l_query += " IF EXISTS(SELECT * FROM POSDB.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'APP_VERSION' AND COLUMN_NAME = 'Assembly_Version') ";
                    l_query += " BEGIN ";
                    l_query += " EXEC ('SELECT TOP(1) Version_No, Release_No , IsNull(Assembly_Version, '''') As Assembly_Version FROM [POSDB].[dbo].[APP_VERSION] ORDER BY ID DESC') ";
                    l_query += " END ";
                    l_query += " ELSE ";
                    l_query += " BEGIN ";
                    l_query += " EXEC ('SELECT TOP(1) Version_No , Release_No , ''Unknow'' As Assembly_Version FROM [POSDB].[dbo].[APP_VERSION] ORDER BY ID DESC') ";
                    l_query += " END ";
                }
                else
                {
                    l_query += " SELECT TOP(1) [VERSION] As Version_No , '' As Release_No , '' As Assembly_Version FROM [POSDB].[dbo].[APP_VERSION] ";
                }

                l_dt_result = LoadTable(l_query);

                l_query = default;

                return l_dt_result;
            }
            catch
            {
                return null;
            }
            finally
            {
                l_dt_result = default;
            }
        }

        public string Update_Version(string p_version, string p_station_id)
        {
            string l_result = "";

            try
            {
                string l_query = "";
                l_query += " UPDATE mDeployDB.dbo.TBStation SET Station_Version = '" + p_version + "' WHERE Station_ID = '" + p_station_id + "' ";

                l_result = ExecSQL(l_query);

                l_query = default;

                return l_result;
            }
            catch(Exception ex)
            {
                return "Error! Update_App_Version : " + ex.Message;
            }
            finally
            {
                l_result = default;
            }
        }

        public string Update_Version_Control(string p_version_control, string p_station_id)
        {
            string l_result = "";

            try
            {
                string l_query = "";
                l_query += " UPDATE mDeployDB.dbo.TBStation SET Station_Version_Control = '" + p_version_control + "' WHERE Station_ID = '" + p_station_id + "' ";

                l_result = ExecSQL(l_query);

                l_query = default;

                return l_result;
            }
            catch (Exception ex)
            {
                return "Error! Update_App_Version : " + ex.Message;
            }
            finally
            {
                l_result = default;
            }
        }

        public string Update_Status(string p_status, string p_station_id)
        {
            string l_result = "";

            try
            {
                string l_query = "";
                l_query += " UPDATE mDeployDB.dbo.TBStation SET Status = " + p_status + " WHERE Station_ID = '" + p_station_id + "' ";

                l_result = ExecSQL(l_query);

                l_query = default;

                return l_result;
            }
            catch (Exception ex)
            {
                return "Error! Update_App_Version : " + ex.Message;
            }
            finally
            {
                l_result = default;
            }
        }

        public string Update_Status_All()
        {
            string l_result = "";

            try
            {
                string l_query = "";
                l_query += " UPDATE mDeployDB.dbo.TBStation SET Station_Status = 0";

                l_result = ExecSQL(l_query);

                l_query = default;

                return l_result;
            }
            catch (Exception ex)
            {
                return "Error! Update_App_Version : " + ex.Message;
            }
            finally
            {
                l_result = default;
            }
        }

        public string Insert_Log(string p_log, string p_station_id)
        {
            string l_result = "";

            try
            {
                string l_query = "";
                l_query += " INSERT INTO mDeployDB.dbo.TBLog (Station_ID, Create_Date_Time, Log_Des) VALUES ('" + p_station_id + "', GETDATE(), '" + p_log + "') ";

                l_result = ExecSQL(l_query);

                l_query = default;

                return l_result;
            }
            catch (Exception ex)
            {
                return "Error! Update_App_Version : " + ex.Message;
            }
            finally
            {
                l_result = default;
            }
        }

        public string Run_Query(string p_query)
        {
            string l_result = "";

            try
            {
                l_result = ExecSQL(p_query);

                return l_result;
            }
            catch (Exception ex)
            {
                return "Error! Run_Query : " + ex.Message;
            }
            finally
            {
                l_result = default;
            }
        }

        #region Dispose
        // Flag: Has Dispose already been called?
        bool disposed = false;
        // Instantiate a SafeHandle instance.
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
                if (c_connection.State != ConnectionState.Open)
                {
                    c_connection.Close();
                }
                c_connection = default;
                c_connection_string = default;
                c_connection_ip = default;
            }

            disposed = true;
        }
        #endregion Dispose
    }
}
