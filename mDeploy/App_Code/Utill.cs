﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using mDeploy.App_Code;

namespace mDeploy.App_Code
{
    class Utill
    {
        public ResultMoveFileParam MoveFile(string p_source_path, string p_target_path)
        {
            ResultMoveFileParam l_result = new ResultMoveFileParam();
            l_result.c_result = "TimeOut";
            l_result.c_list_files_target = new List<string>();
            l_result.c_list_files_source = new List<string>();

            try
            {
                DirectoryInfo l_source_path = new DirectoryInfo(p_source_path);
                DirectoryInfo l_target_path = new DirectoryInfo(p_target_path);

                if (l_source_path.Exists)
                {
                    Thread b_thread = new Thread(() => {
                        l_result = CopyAll(l_source_path, l_target_path);
                    });

                    b_thread.Name = "CopyAll";
                    b_thread.Start();

                    for (int b_int = 0; b_int < 180; b_int++)
                    {
                        if (l_result.c_result == "OK")
                        {
                            b_thread.Abort();
                            break;
                        }

                        Thread.Sleep(1000);
                    }

                    b_thread = default;
                }

                l_source_path = default;
                l_target_path = default;

                return l_result;
            }
            catch(Exception ex)
            {
                l_result.c_result = "Error! MoveFile : " + ex.Message;
                return l_result;
            }
            finally
            {
                l_result = default;
            }
        }

        public ResultMoveFileParam CopyAll(DirectoryInfo p_form_dir, DirectoryInfo p_to_dir)
        {
            ResultMoveFileParam l_result = new ResultMoveFileParam();

            try
            {
                l_result.c_result = "OK";
                l_result.c_list_files_target = new List<string>();
                l_result.c_list_files_source = new List<string>();

                foreach (FileInfo b_fi in p_form_dir.GetFiles())
                {
                    string b_combine_path = Path.Combine(p_to_dir.FullName, b_fi.Name);
                    b_fi.CopyTo(b_combine_path, true);
                    l_result.c_list_files_target.Add(b_combine_path);
                    l_result.c_list_files_source.Add(b_fi.FullName);

                    b_combine_path = default;
                }

                foreach (DirectoryInfo b_di_source_sub_dir in p_form_dir.GetDirectories())
                {
                    DirectoryInfo b_next_target_sub_dir = p_to_dir.CreateSubdirectory(b_di_source_sub_dir.Name);
                    ResultMoveFileParam b_result = new ResultMoveFileParam();

                    b_result = CopyAll(b_di_source_sub_dir, b_next_target_sub_dir);

                    l_result.c_list_files_target.AddRange(b_result.c_list_files_target);
                    l_result.c_list_files_source.AddRange(b_result.c_list_files_source);

                    if (b_result.c_result != "OK")
                    {
                        l_result.c_result = b_result.c_result;
                        return l_result;
                    }
                }

                return l_result;
            }
            catch(Exception ex)
            {
                l_result.c_result = "Error! MoveFile : " + ex.Message;
                return l_result;
            }
            finally
            {
                l_result = default;
            }
        }

        public string CheckFile(List<string> p_source, List<string> p_target)
        {
            string l_result = "OK";

            try
            {
                if (l_result == "OK")
                {
                    if (p_source.Count != p_target.Count)
                    {
                        l_result = "p_source.Count[" + p_source.Count + "] != p_target.Count[" + p_target.Count + "]";
                    }
                }

                if (l_result == "OK")
                {
                    for (int i = 0; i < p_target.Count; i++)
                    {
                        FileInfo b_source_info = new FileInfo(p_source[i]);
                        FileInfo b_target_info = new FileInfo(p_target[i]);

                        if (b_source_info.Length != b_target_info.Length)
                        {
                            l_result = "Size != " + b_target_info.FullName + " Size : " + b_target_info.Length.ToString();
                            return l_result;
                        }

                        if (b_source_info.LastWriteTime != b_target_info.LastWriteTime)
                        {
                            l_result = "CreationTime != " + b_target_info.FullName + " CreationTime : " + b_target_info.LastWriteTime.ToString();
                            return l_result;
                        }
                    }
                }

                return l_result;
            }
            catch
            {
                return "Error";
            }
            finally
            { 
            
            }
        }

        public void log(string p_log_name, string p_log)
        {
            try
            {
                string l_path = Application.StartupPath + "/Log/" + p_log_name + "_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                string l_data = DateTime.Now.ToString("HH:mm:ss.ffff") + " " + p_log + Environment.NewLine;

                if (new FileInfo(l_path).Directory.Exists == false)
                {
                    Directory.CreateDirectory(new FileInfo(l_path).Directory.FullName);
                }

                File.AppendAllText(l_path, l_data);
            }
            catch(Exception ex)
            {

            }
        }
    }
}
