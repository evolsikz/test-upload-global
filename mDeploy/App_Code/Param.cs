﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mDeploy.App_Code
{
    public class ReportProgressParam
    {
        public int c_index { get; set; }
        public bool c_coco { get; set; }
        public bool c_status { get; set; }
        public string c_station_id { get; set; }
        public string c_station_ip { get; set; }
        public string c_ping_status { get; set; }
        public string c_desc { get; set; }
        public string c_version { get; set; }
        public string c_version_control { get; set; }
        public ResultMoveFileParam c_result_move_file { get; set; }
    }

    public class CheckFilesParam
    {
        public int c_index { get; set; }
        public bool c_coco { get; set; }
        public string c_station_id { get; set; }
        public string c_station_ip { get; set; }
        public string c_version { get; set; }
        public List<string> c_list_files_target { get; set; }
        public List<string> c_list_files_source { get; set; }
    }

    public class ResultMoveFileParam
    { 
        public string c_result { get; set; }
        public List<string> c_list_files_target { get; set; }
        public List<string> c_list_files_source { get; set; }
    }

    public class CopyAllParam
    {
        public System.IO.DirectoryInfo c_source_path;
        public System.IO.DirectoryInfo c_target_path;
    }
}
