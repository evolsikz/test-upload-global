﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using mDeploy.App_Code;

namespace mDeploy
{
    public partial class frmMain : Form
    {
        DAL f_dal;
        DataTable f_dt_main;
        BackgroundWorker f_bw;
        BackgroundWorker f_bw_cf;
        List<CheckFilesParam> f_fp;
        //testupload
        //testupload+gitignore
        //testupload+gitignoreV2

        string f_is_prd;
        string f_bussiness_type;
        string f_fail;

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            f_dal = new DAL();
            f_dal.Connection_IP = "127.0.0.1";
            f_dal.Connection_Open();

            {
                this.cmbBussiness_Type.SelectedIndex = 0;
                this.f_bussiness_type = "'" + this.cmbBussiness_Type.Text + "'";
                this.f_is_prd = "'True'";
                this.f_fail = " And Status = 'False'";
                this.cbFail.Checked = true;
            }

            {
                this.refesh();
            }

            {
                this.f_bw = new BackgroundWorker();
                this.f_bw.WorkerReportsProgress = true;
                this.f_bw.WorkerSupportsCancellation = true;

                this.f_bw.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
                this.f_bw.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
                this.f_bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);

                this.f_bw_cf = new BackgroundWorker();
                this.f_bw_cf.WorkerReportsProgress = true;
                this.f_bw_cf.WorkerSupportsCancellation = true;

                this.f_bw_cf.DoWork += new DoWorkEventHandler(bw_check_file_DoWork);
                this.f_bw_cf.ProgressChanged += new ProgressChangedEventHandler(bw_check_file_ProgressChanged);
                this.f_bw_cf.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_check_file_RunWorkerCompleted);
            }
        }

        private void cbTest_CheckStateChanged(object sender, EventArgs e)
        {
            CheckBox l_check_box = (CheckBox)sender;

            if (l_check_box.CheckState == CheckState.Checked)
            {
                this.f_is_prd = "'False'";
            }
            else
            {
                this.f_is_prd = "'True'";
            }
            this.refesh();

            l_check_box = default;
        }

        private void cbFail_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox l_check_box = (CheckBox)sender;

            if (l_check_box.CheckState == CheckState.Checked)
            {
                this.f_fail = " And Status = 'False'";
            }
            else
            {
                this.f_fail = " And Status <> 'False'";
            }
            this.refesh();

            l_check_box = default;
        }

        private void cmbBussiness_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.f_dt_main != null)
            {
                this.f_bussiness_type = "'" + this.cmbBussiness_Type.Text + "'";
                this.refesh();
            }
        }

        private void refesh()
        {
            this.f_dt_main = f_dal.GetStation();

            if (this.f_dt_main != null)
            {
                DataTable b_dt = null;
                DataRow[] b_dr = this.f_dt_main.Select("IsPrd = " + this.f_is_prd + " And Bussiness_Type = " + this.f_bussiness_type + "" + this.f_fail);
                if (b_dr.Length > 0)
                {
                    b_dt = b_dr.CopyToDataTable();
                }
                else
                {
                    b_dt = null;
                }

                this.dgvMain.AutoGenerateColumns = false;
                this.dgvMain.DataSource = b_dt;

                b_dt = default;
                b_dr = default;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (this.f_bw.IsBusy != true)
            {
                DataTable l_dt = ((DataTable)(this.dgvMain.DataSource)).Copy();
                this.f_fp = new List<CheckFilesParam>();

                this.pgbMain.Value = 0;
                this.pgbMain.Maximum = l_dt.Rows.Count;
                this.pgbCheck.Value = 0;

                this.lblFilesTranfer.Text = "Tranfer : ";
                this.lblCheck.Text = "Check : ";
                this.tmCheckFile.Enabled = true;

                this.f_bw.RunWorkerAsync(argument: l_dt);

                this.dgvMain.DataSource = l_dt;
                l_dt = default;
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (this.f_bw.WorkerSupportsCancellation == true)
            {
                this.f_bw.CancelAsync();
            }

            this.f_fp = new List<CheckFilesParam>();
            this.tmCheckFile.Enabled = false;
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker l_worker = sender as BackgroundWorker;
            DataTable l_dt = (DataTable)e.Argument;

            for (int i = 0; i < l_dt.Rows.Count; i++)
            {
                if (l_worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    ReportProgressParam b_rpp = new ReportProgressParam();
                    ResultMoveFileParam b_move;
                    b_rpp.c_index = i;
                    b_rpp.c_ping_status = "";
                    b_rpp.c_desc = "OK";

                    string b_ip;
                    string b_station_id;
                    string b_station_name;

                    string b_version_no;
                    string b_release_no;
                    string b_assembly_version;

                    string b_depot;
                    string b_sitename;

                    string b_path_source;
                    string b_path_target;

                    string b_bussiness_type;

                    Ping b_ping;
                    PingReply b_ping_reply;

                    DAL b_dal;
                    Utill b_utill;

                    DataTable b_dt;

                    bool b_coco;
                    bool b_connect_db_result;

                    b_dal = new DAL();
                    b_dt = new DataTable();
                    b_utill = new Utill();

                    b_ip = l_dt.Rows[i]["Station_IP"].ToString().Replace(" ", "");
                    b_station_id = l_dt.Rows[i]["Station_ID"].ToString();
                    b_station_name = l_dt.Rows[i]["Station_Name"].ToString();
                    b_bussiness_type = l_dt.Rows[i]["Bussiness_Type"].ToString();

                    b_ping = new Ping();
                    b_ping_reply = b_ping.Send(b_ip, 2000);

                    b_rpp.c_ping_status = b_ping_reply.Status.ToString();
                    b_rpp.c_station_id = b_station_id;
                    b_rpp.c_version = "";
                    b_rpp.c_version_control = "";
                    b_rpp.c_desc = "Start";
                    l_worker.ReportProgress(i + 1, b_rpp);

                    if (b_ping_reply.Status != IPStatus.Success)
                    {
                        b_rpp.c_desc = "Ping Not Success";
                        l_worker.ReportProgress(i + 1, b_rpp);
                        b_utill.log("TF", "[" + b_station_id + "]" + " Ping Not Success");
                        continue;
                    }

                    b_dal.Connection_IP = b_ip;
                    b_connect_db_result = b_dal.Connection_Open_Result();
                    b_rpp.c_station_ip = b_ip;
                    b_utill.log("TF", "[" + b_station_id + "]" + " Start");

                    if (b_connect_db_result == false)
                    {
                        b_rpp.c_desc = "ไม่สามารถติดต่อฐานข้อมูลได้";
                        l_worker.ReportProgress(i + 1, b_rpp);
                        b_utill.log("TF", "[" + b_station_id + "]" + " ไม่สามารถติดต่อฐานข้อมูลได้");
                        continue;
                    }

                    b_dt = b_dal.GetApp_Data();

                    if (b_dt != null)
                    {
                        if (b_dt.Rows.Count > 0)
                        {
                            b_depot = b_dt.Rows[0]["DEPOT"].ToString();
                            b_sitename = b_dt.Rows[0]["SITENAME"].ToString();
                        }
                        else
                        {
                            b_rpp.c_desc = "ไม่พบข้อมูล APP_DATA";
                            l_worker.ReportProgress(i + 1, b_rpp);
                            b_utill.log("TF", "[" + b_station_id + "]" + " ไม่พบข้อมูล APP_DATA");
                            continue;
                        }
                    }
                    else
                    {
                        b_rpp.c_desc = "Error! APP_DATA";
                        l_worker.ReportProgress(i + 1, b_rpp);
                        b_utill.log("TF", "[" + b_station_id + "]" + " Error! APP_DATA");
                        continue;
                    }

                    b_dt = b_dal.GetIsCoco();

                    if (b_dt != null)
                    {
                        if (b_dt.Rows.Count > 0)
                        {
                            if (b_dt.Rows[0]["CONFIG_VALUE"].ToString() == "1")
                            {
                                b_coco = false;
                                b_rpp.c_coco = b_coco;
                            }
                            else
                            {
                                b_coco = true;
                                b_rpp.c_coco = b_coco;
                            }
                        }
                        else
                        {
                            b_rpp.c_desc = "ไม่พบข้อมูล APP_CONFIG";
                            l_worker.ReportProgress(i + 1, b_rpp);
                            b_utill.log("TF", "[" + b_station_id + "]" + " ไม่พบข้อมูล APP_CONFIG");
                            continue;
                        }
                    }
                    else
                    {
                        b_rpp.c_desc = "Error! APP_CONFIG";
                        l_worker.ReportProgress(i + 1, b_rpp);
                        b_utill.log("TF", "[" + b_station_id + "]" + " Error! APP_CONFIG");
                        continue;
                    }

                    if (b_depot != b_station_id)
                    {
                        b_rpp.c_desc = "DEPOT[" + b_depot + "] != Station_ID[" + b_station_id + "]";
                        l_worker.ReportProgress(i + 1, b_rpp);
                        b_utill.log("TF", "[" + b_station_id + "]" + " DEPOT[" + b_depot + "] != Station_ID[" + b_station_id + "]");
                        continue;
                    }

                    b_dt = b_dal.GetVersion(b_bussiness_type);

                    if (b_dt != null)
                    {
                        if (b_dt.Rows.Count > 0)
                        {
                            b_version_no = b_dt.Rows[0]["Version_No"].ToString();
                            b_release_no = b_dt.Rows[0]["Release_No"].ToString();
                            b_assembly_version = b_dt.Rows[0]["Assembly_Version"].ToString();

                            b_rpp.c_version = b_version_no;
                            b_rpp.c_version_control = b_assembly_version;
                            b_rpp.c_desc = "Version : " + b_version_no;
                            l_worker.ReportProgress(i + 1, b_rpp);
                        }
                        else
                        {
                            b_rpp.c_desc = "ไม่พบข้อมูล APP_VERSION";
                            l_worker.ReportProgress(i + 1, b_rpp);
                            b_utill.log("TF", "[" + b_station_id + "]" + " ไม่พบข้อมูล APP_VERSION");
                            continue;
                        }
                    }
                    else
                    {
                        b_rpp.c_desc = "Error! APP_VERSION";
                        l_worker.ReportProgress(i + 1, b_rpp);
                        b_utill.log("TF", "[" + b_station_id + "]" + " Error! APP_VERSION");
                        continue;
                    }

                    if (b_bussiness_type == "OIL")
                    {
                        b_path_source = "";
                        b_path_target = @"\\{0}\C$\inetpub\wwwroot\PTT_OIL"; //<<<<<<<<<<<< อย่าลืมแก้ Path
                        b_path_target = string.Format(b_path_target, b_ip);
                        string b_version_remove = b_version_no.Substring(0, b_version_no.Length - 2);

                        if (b_version_remove == "9.6")
                        {
                            if (b_coco == true)
                            {
                                b_path_source = Application.StartupPath + @"\Deploy\OIL\Drive_C\" + b_version_remove + "_C";
                            }
                            else
                            {
                                b_path_source = Application.StartupPath + @"\Deploy\OIL\Drive_C\" + b_version_remove + "_D";
                            }
                        }
                        else if (b_version_remove == "10.2")
                        {
                            if (b_coco == true)
                            {
                                b_path_source = Application.StartupPath + @"\Deploy\OIL\Drive_C\" + b_version_remove + "_C";
                            }
                            else
                            {
                                b_path_source = Application.StartupPath + @"\Deploy\OIL\Drive_C\" + b_version_remove + "_D";
                            }
                        }
                        else
                        {
                            b_rpp.c_desc = "ไม่อยู่ในเวอร์ชั่นที่จะทำการอัพเดท";
                            l_worker.ReportProgress(i + 1, b_rpp);
                            b_utill.log("TF", "[" + b_station_id + "]" + " ไม่อยู่ในเวอร์ชั่นที่จะทำการอัพเดท");
                        }
                    }
                    else
                    {
                        b_path_source = "";
                        b_path_target = @"\\{0}\D$\WEB\PTT_NGV_X";
                        b_path_target = string.Format(b_path_target, b_ip);

                        b_path_source = Application.StartupPath + @"\Deploy\NGV\Drive_D\PTT_NGV";
                    }

                    if (b_path_source != "")
                    {
                        if (Directory.Exists(b_path_source) == false)
                        {
                            b_rpp.c_desc = @b_path_source + " Not Exists";
                            l_worker.ReportProgress(i + 1, b_rpp);
                            b_utill.log("TF", "[" + b_station_id + "]" + " " + @b_path_source + " Not Exists");
                            continue;
                        }
                        else if (Directory.Exists(b_path_target) == false)
                        {
                            b_rpp.c_desc = @b_path_target + " Not Exists";
                            l_worker.ReportProgress(i + 1, b_rpp);
                            b_utill.log("TF", "[" + b_station_id + "]" + " " + @b_path_source + " Not Exists");
                            continue;
                        }
                        else
                        {

                            b_move = new ResultMoveFileParam();
                            b_move = b_utill.MoveFile(b_path_source, b_path_target);

                            b_rpp.c_desc = b_move.c_result;
                            b_rpp.c_result_move_file = b_move;

                            l_worker.ReportProgress(i + 1, b_rpp);
                            b_utill.log("TF", "[" + b_station_id + "]" + " " + "วางไฟล์ " + b_move.c_result);
                        }
                    }
                    else
                    {
                        continue;
                    }

                    b_ip = default;
                    b_station_id = default;
                    b_station_name = default;

                    b_version_no = default;
                    b_release_no = default;
                    b_assembly_version = default;

                    b_depot = default;
                    b_sitename = default;

                    b_path_source = default;
                    b_path_target = default;

                    b_ping = default;
                    b_ping_reply = default;

                    b_dal.Connection_Close();
                    b_dal.Dispose();
                    b_dal = default;
                    b_utill = default;

                    b_dt = default;

                    b_coco = default;
                    b_connect_db_result = default;

                    b_move = default;
                    b_rpp = default;
                }
            }
        }

        // This event handler updates the progress.
        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                this.pgbMain.Value = (e.ProgressPercentage);
                ReportProgressParam b_rpp = (ReportProgressParam)e.UserState;

                this.dgvMain.Rows[b_rpp.c_index].Cells["cPing"].Value = b_rpp.c_ping_status;
                this.dgvMain.Rows[b_rpp.c_index].Cells["cDesc_FilesMove"].Value = b_rpp.c_desc;

                if (b_rpp.c_version != "")
                {
                    this.dgvMain.Rows[b_rpp.c_index].Cells["cStation_Version"].Value = b_rpp.c_version;
                    this.f_dal.Update_Version(b_rpp.c_version, b_rpp.c_station_id);
                }

                if (b_rpp.c_version_control != "")
                {
                    this.dgvMain.Rows[b_rpp.c_index].Cells["cStation_Version_Control"].Value = b_rpp.c_version_control;
                    this.f_dal.Update_Version_Control(b_rpp.c_version_control, b_rpp.c_station_id);
                }

                if (b_rpp.c_result_move_file != null)
                {
                    if (b_rpp.c_result_move_file.c_result == "OK")
                    {
                        CheckFilesParam b_param = new CheckFilesParam();

                        b_param.c_index = b_rpp.c_index;
                        b_param.c_coco = b_rpp.c_coco;
                        b_param.c_version = b_rpp.c_version;
                        b_param.c_station_id = b_rpp.c_station_id;
                        b_param.c_station_ip = b_rpp.c_station_ip;
                        b_param.c_list_files_target = b_rpp.c_result_move_file.c_list_files_target;
                        b_param.c_list_files_source = b_rpp.c_result_move_file.c_list_files_source;

                        this.f_fp.Add(b_param);
                    }
                }


                this.lblFilesTranfer.Text = "Tranfer : " + e.ProgressPercentage.ToString() + "/" + this.pgbMain.Maximum.ToString();
                this.f_dal.Insert_Log(b_rpp.c_desc, b_rpp.c_station_id);
            }
            catch (Exception ex)
            {
                string test = "";
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                MessageBox.Show("Canceled");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("Error: " + e.Error.Message);
            }
            else
            {
                MessageBox.Show("Done!");
            }
        }

        private void bw_check_file_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker l_worker = sender as BackgroundWorker;
            CheckFilesParam b_check_file;

            try
            {
                Utill b_utill = new Utill();
                DAL b_dal = new DAL();
                ReportProgressParam b_rpp = new ReportProgressParam();

                string b_result = "";

                b_check_file = new CheckFilesParam();
                b_check_file = (CheckFilesParam)e.Argument;
                b_result = b_utill.CheckFile(b_check_file.c_list_files_source, b_check_file.c_list_files_target);
                
                b_rpp.c_index = b_check_file.c_index;
                b_rpp.c_station_id = b_check_file.c_station_id;
                b_rpp.c_desc = "Check File " + b_result;
                b_rpp.c_status = false;

                l_worker.ReportProgress(1, b_rpp);
                
                b_dal.Connection_IP = b_check_file.c_station_ip;
                b_dal.Connection_Open();

                b_utill.log("CF", "[" + b_check_file.c_station_id + "]" + " Check File " + b_result);

                if (b_result == "OK")
                {
                    string b_path;
                    string b_version_remove = b_check_file.c_version.Substring(0, b_check_file.c_version.Length - 2);

                    if (b_version_remove == "9.6")
                    {
                        if (b_check_file.c_coco == true)
                        {
                            b_path = Application.StartupPath + @"\Deploy\OIL\Script\9.6_C\1_Fix_CoCo_964.sql";
                        }
                        else
                        {
                            b_path = Application.StartupPath + @"\Deploy\OIL\Script\9.6_D\1_Fix_Dealer_964.sql";
                        }
                    }
                    else if (b_version_remove == "10.2")
                    {
                        if (b_check_file.c_coco == true)
                        {
                            b_path = Application.StartupPath + @"\Deploy\OIL\Script\10.2_C\1_Fix_CoCo_10p2p1.sql";
                        }
                        else
                        {
                            b_path = Application.StartupPath + @"\Deploy\OIL\Script\10.2_D\1_Fix_Dealer_10p2p1.sql";
                        }
                    }
                    else
                    {
                        b_path = "";
                    }

                    if (b_path != "")
                    {
                        FileInfo b_file_info = new FileInfo(b_path);
                        if (b_file_info.Exists)
                        {
                            string b_result_run_query = "";
                            string b_script = b_file_info.OpenText().ReadToEnd();
                            string b_script_replace = Regex.Replace(b_script, @"/\*(.|\n)*?\*/", "");
                            IEnumerable<string> b_list = Regex.Split(b_script, @"^\s*GO\s*$|^\s*GO", RegexOptions.Multiline | RegexOptions.IgnoreCase);

                            foreach (string b_str in b_list)
                            {
                                if (b_str != "")
                                {
                                    b_result_run_query = b_dal.Run_Query(b_str);

                                    if (b_result_run_query != "OK")
                                    {
                                        b_result = "Run Query Fail " + b_result_run_query;
                                        break;
                                    }
                                }
                            }

                            b_result_run_query = default;
                            b_script = default;
                            b_script_replace = default;
                            b_list = default;
                        }
                        b_file_info = default;
                    }
                    else
                    {
                        b_result = "ไม่พบ Version สำหรับ Run Query";
                    }

                    b_path = default;
                }

                if (b_result == "OK")
                {
                    b_rpp.c_status = true;
                    b_rpp.c_desc = "Success";
                    l_worker.ReportProgress(2, b_rpp);
                }
                else
                {
                    b_rpp.c_desc = "Run Query Error! : " + b_result;
                    l_worker.ReportProgress(2, b_rpp);
                }

                b_utill = default;
                b_check_file = default;
                b_result = default;
                b_dal.Connection_Close();
                b_dal.Dispose();
                b_dal = default;
            }
            catch
            {

            }
            finally
            { 
            
            }
        }

        private void bw_check_file_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                this.pgbCheck.Value = (e.ProgressPercentage);
                ReportProgressParam b_rpp = (ReportProgressParam)e.UserState;

                this.dgvMain.Rows[b_rpp.c_index].Cells["cDesc_CheckFiles"].Value = b_rpp.c_desc;
                this.dgvMain.Rows[b_rpp.c_index].Cells["cStatus"].Value = b_rpp.c_status;
                this.lblCheck.Text = "Check : " + b_rpp.c_station_id;

                this.f_dal.Insert_Log(b_rpp.c_desc, b_rpp.c_station_id);
                if (b_rpp.c_status == true)
                {
                    this.f_dal.Update_Status("1", b_rpp.c_station_id);
                }
            }
            catch (Exception ex)
            {
                string test = "";
            }
        }

        private void bw_check_file_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.f_fp.RemoveAt(0);
        }

        private void tmCheckFile_Tick(object sender, EventArgs e)
        {
            if (this.f_fp.Count > 0)
            {
                if (this.f_bw_cf.IsBusy != true)
                {
                    CheckFilesParam b_cfp = new CheckFilesParam();
                    b_cfp = this.f_fp[0];

                    this.pgbCheck.Value = 0;
                    this.pgbCheck.Maximum = 2;

                    this.f_bw_cf.RunWorkerAsync(argument: b_cfp);

                    b_cfp = default;
                }
            }

            if (this.f_bw.IsBusy == false && this.f_fp.Count == 0)
            {
                this.tmCheckFile.Enabled = false;
            }
        }
    }
}
