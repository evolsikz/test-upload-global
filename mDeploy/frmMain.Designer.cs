﻿namespace mDeploy
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvMain = new System.Windows.Forms.DataGridView();
            this.cStatuin_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cStation_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cStation_IP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cStation_Version = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cStation_Version_Control = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPing = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cDesc_FilesMove = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cDesc_CheckFiles = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cIsPrd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cBussiness_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnStart = new System.Windows.Forms.Button();
            this.cbTest = new System.Windows.Forms.CheckBox();
            this.pgbMain = new System.Windows.Forms.ProgressBar();
            this.btnStop = new System.Windows.Forms.Button();
            this.cmbBussiness_Type = new System.Windows.Forms.ComboBox();
            this.tmCheckFile = new System.Windows.Forms.Timer(this.components);
            this.cbFail = new System.Windows.Forms.CheckBox();
            this.lblFilesTranfer = new System.Windows.Forms.Label();
            this.lblCheck = new System.Windows.Forms.Label();
            this.pgbCheck = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvMain
            // 
            this.dgvMain.AllowUserToAddRows = false;
            this.dgvMain.AllowUserToDeleteRows = false;
            this.dgvMain.AllowUserToResizeColumns = false;
            this.dgvMain.AllowUserToResizeRows = false;
            this.dgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cStatuin_ID,
            this.cStation_Name,
            this.cStation_IP,
            this.cStation_Version,
            this.cStation_Version_Control,
            this.cPing,
            this.cDesc_FilesMove,
            this.cDesc_CheckFiles,
            this.cStatus,
            this.cIsPrd,
            this.cBussiness_Type});
            this.dgvMain.Location = new System.Drawing.Point(12, 12);
            this.dgvMain.Name = "dgvMain";
            this.dgvMain.ReadOnly = true;
            this.dgvMain.RowHeadersVisible = false;
            this.dgvMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMain.Size = new System.Drawing.Size(860, 346);
            this.dgvMain.TabIndex = 0;
            // 
            // cStatuin_ID
            // 
            this.cStatuin_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cStatuin_ID.DataPropertyName = "Station_ID";
            this.cStatuin_ID.FillWeight = 40F;
            this.cStatuin_ID.HeaderText = "รหัสสถานี";
            this.cStatuin_ID.Name = "cStatuin_ID";
            this.cStatuin_ID.ReadOnly = true;
            // 
            // cStation_Name
            // 
            this.cStation_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cStation_Name.DataPropertyName = "Station_Name";
            this.cStation_Name.HeaderText = "ชื่อสถานี";
            this.cStation_Name.Name = "cStation_Name";
            this.cStation_Name.ReadOnly = true;
            // 
            // cStation_IP
            // 
            this.cStation_IP.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cStation_IP.DataPropertyName = "Station_IP";
            this.cStation_IP.FillWeight = 60F;
            this.cStation_IP.HeaderText = "IP";
            this.cStation_IP.Name = "cStation_IP";
            this.cStation_IP.ReadOnly = true;
            // 
            // cStation_Version
            // 
            this.cStation_Version.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cStation_Version.DataPropertyName = "Station_Version";
            this.cStation_Version.FillWeight = 60F;
            this.cStation_Version.HeaderText = "Version";
            this.cStation_Version.Name = "cStation_Version";
            this.cStation_Version.ReadOnly = true;
            // 
            // cStation_Version_Control
            // 
            this.cStation_Version_Control.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cStation_Version_Control.DataPropertyName = "Station_Version_Control";
            this.cStation_Version_Control.FillWeight = 60F;
            this.cStation_Version_Control.HeaderText = "Version Control";
            this.cStation_Version_Control.Name = "cStation_Version_Control";
            this.cStation_Version_Control.ReadOnly = true;
            // 
            // cPing
            // 
            this.cPing.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cPing.FillWeight = 20F;
            this.cPing.HeaderText = "Ping";
            this.cPing.Name = "cPing";
            this.cPing.ReadOnly = true;
            // 
            // cDesc_FilesMove
            // 
            this.cDesc_FilesMove.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cDesc_FilesMove.HeaderText = "วางไฟล์";
            this.cDesc_FilesMove.Name = "cDesc_FilesMove";
            this.cDesc_FilesMove.ReadOnly = true;
            // 
            // cDesc_CheckFiles
            // 
            this.cDesc_CheckFiles.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cDesc_CheckFiles.HeaderText = "ตรวจสอบไฟล์";
            this.cDesc_CheckFiles.Name = "cDesc_CheckFiles";
            this.cDesc_CheckFiles.ReadOnly = true;
            // 
            // cStatus
            // 
            this.cStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cStatus.DataPropertyName = "Status";
            this.cStatus.FillWeight = 50F;
            this.cStatus.HeaderText = "Status";
            this.cStatus.Name = "cStatus";
            this.cStatus.ReadOnly = true;
            // 
            // cIsPrd
            // 
            this.cIsPrd.DataPropertyName = "IsPrd";
            this.cIsPrd.HeaderText = "IsPrd";
            this.cIsPrd.Name = "cIsPrd";
            this.cIsPrd.ReadOnly = true;
            this.cIsPrd.Visible = false;
            // 
            // cBussiness_Type
            // 
            this.cBussiness_Type.DataPropertyName = "Bussiness_Type";
            this.cBussiness_Type.HeaderText = "Bussiness_Type";
            this.cBussiness_Type.Name = "cBussiness_Type";
            this.cBussiness_Type.ReadOnly = true;
            this.cBussiness_Type.Visible = false;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(797, 426);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // cbTest
            // 
            this.cbTest.AutoSize = true;
            this.cbTest.Location = new System.Drawing.Point(139, 430);
            this.cbTest.Name = "cbTest";
            this.cbTest.Size = new System.Drawing.Size(47, 17);
            this.cbTest.TabIndex = 2;
            this.cbTest.Text = "Test";
            this.cbTest.UseVisualStyleBackColor = true;
            this.cbTest.CheckStateChanged += new System.EventHandler(this.cbTest_CheckStateChanged);
            // 
            // pgbMain
            // 
            this.pgbMain.Location = new System.Drawing.Point(12, 364);
            this.pgbMain.Name = "pgbMain";
            this.pgbMain.Size = new System.Drawing.Size(860, 23);
            this.pgbMain.TabIndex = 3;
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(716, 426);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 4;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // cmbBussiness_Type
            // 
            this.cmbBussiness_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBussiness_Type.FormattingEnabled = true;
            this.cmbBussiness_Type.Items.AddRange(new object[] {
            "OIL",
            "NGV"});
            this.cmbBussiness_Type.Location = new System.Drawing.Point(12, 428);
            this.cmbBussiness_Type.Name = "cmbBussiness_Type";
            this.cmbBussiness_Type.Size = new System.Drawing.Size(121, 21);
            this.cmbBussiness_Type.TabIndex = 5;
            this.cmbBussiness_Type.SelectedIndexChanged += new System.EventHandler(this.cmbBussiness_Type_SelectedIndexChanged);
            // 
            // tmCheckFile
            // 
            this.tmCheckFile.Tick += new System.EventHandler(this.tmCheckFile_Tick);
            // 
            // cbFail
            // 
            this.cbFail.AutoSize = true;
            this.cbFail.Location = new System.Drawing.Point(192, 430);
            this.cbFail.Name = "cbFail";
            this.cbFail.Size = new System.Drawing.Size(42, 17);
            this.cbFail.TabIndex = 6;
            this.cbFail.Text = "Fail";
            this.cbFail.UseVisualStyleBackColor = true;
            this.cbFail.CheckedChanged += new System.EventHandler(this.cbFail_CheckedChanged);
            // 
            // lblFilesTranfer
            // 
            this.lblFilesTranfer.AutoSize = true;
            this.lblFilesTranfer.Location = new System.Drawing.Point(19, 369);
            this.lblFilesTranfer.Name = "lblFilesTranfer";
            this.lblFilesTranfer.Size = new System.Drawing.Size(47, 13);
            this.lblFilesTranfer.TabIndex = 7;
            this.lblFilesTranfer.Text = "Tranfer :";
            // 
            // lblCheck
            // 
            this.lblCheck.AutoSize = true;
            this.lblCheck.BackColor = System.Drawing.Color.Transparent;
            this.lblCheck.ForeColor = System.Drawing.Color.Black;
            this.lblCheck.Location = new System.Drawing.Point(19, 398);
            this.lblCheck.Name = "lblCheck";
            this.lblCheck.Size = new System.Drawing.Size(44, 13);
            this.lblCheck.TabIndex = 9;
            this.lblCheck.Text = "Check :";
            // 
            // pgbCheck
            // 
            this.pgbCheck.Location = new System.Drawing.Point(12, 393);
            this.pgbCheck.Name = "pgbCheck";
            this.pgbCheck.Size = new System.Drawing.Size(860, 23);
            this.pgbCheck.TabIndex = 8;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.lblCheck);
            this.Controls.Add(this.pgbCheck);
            this.Controls.Add(this.lblFilesTranfer);
            this.Controls.Add(this.cbFail);
            this.Controls.Add(this.cmbBussiness_Type);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.pgbMain);
            this.Controls.Add(this.cbTest);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.dgvMain);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(900, 500);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(900, 500);
            this.Name = "frmMain";
            this.Text = "mDeploy";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMain;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.CheckBox cbTest;
        private System.Windows.Forms.ProgressBar pgbMain;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.ComboBox cmbBussiness_Type;
        private System.Windows.Forms.Timer tmCheckFile;
        private System.Windows.Forms.CheckBox cbFail;
        private System.Windows.Forms.DataGridViewTextBoxColumn cStatuin_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn cStation_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn cStation_IP;
        private System.Windows.Forms.DataGridViewTextBoxColumn cStation_Version;
        private System.Windows.Forms.DataGridViewTextBoxColumn cStation_Version_Control;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPing;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDesc_FilesMove;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDesc_CheckFiles;
        private System.Windows.Forms.DataGridViewTextBoxColumn cStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn cIsPrd;
        private System.Windows.Forms.DataGridViewTextBoxColumn cBussiness_Type;
        private System.Windows.Forms.Label lblFilesTranfer;
        private System.Windows.Forms.Label lblCheck;
        private System.Windows.Forms.ProgressBar pgbCheck;
    }
}

